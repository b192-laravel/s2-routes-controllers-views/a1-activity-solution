<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// the code below is the middleware used to get user_id, to verify user or anything that has to do with authentication.
use Illuminate\Support\Facades\Auth;
// Import Post model since we are going to need to use database.
use App\Models\Post;

class PostController extends Controller
{
    public function create()
    {
        return view('posts.create');
    }

    // $request is the filled-up form
    public function store(Request $request)
    {
        // if there is an authenticated user
        if(Auth::user()) {
            // create a new Post object from the Post model
            $post = new Post;

            // define the properties of the $post object using received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            // get the id of the authenticated user and set it as the value of the user_id column
            $post->user_id = (Auth::user()->id);
            // save the post object to the database
            $post->save();

            return redirect('/posts');
        }else {
            return redirect('/login');
        }
    }

    public function index()
    {
        // get all posts from the database

        $posts = Post::all();
        return view('posts.index')->with('posts', $posts);
    }

    public function welcome()
    {
    
        $posts = Post::inRandomOrder()->limit(3)->get();
        return view('welcome')->with('posts', $posts);
    }
}






